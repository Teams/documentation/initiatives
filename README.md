# Documentation Team Initiatives

This project is for tracking projects and initiatives for the GNOME documentation team.
Use issues on this project to track projects that aren't tied to any particular repository,
or to create tracking issues linking to other issues.
For issues on specific documents, use the respective repositories.
That could be [gnome-user-docs](https://gitlab.gnome.org/GNOME/gnome-user-docs/),
[gnome-devel-docs](https://gitlab.gnome.org/GNOME/gnome-devel-docs),
or individual app repos.

Here are some useful links:

* [Initiatives tracked as issues in this project](https://gitlab.gnome.org/Teams/documentation/initiatives/-/issues)
* [Documentation team wiki](https://wiki.gnome.org/DocumentationProject/)
* [User docs issues](https://gitlab.gnome.org/groups/GNOME/-/issues?label_name%5B%5D=8.+User+Docs)
* [User docs merge requests](https://gitlab.gnome.org/groups/GNOME/-/merge_requests?label_name%5B%5D=8.+User+Docs)
* [Developer docs issues](https://gitlab.gnome.org/groups/GNOME/-/issues?label_name%5B%5D=8.+Developer+Docs)
* [Developer docs merge requests](https://gitlab.gnome.org/groups/GNOME/-/merge_requests?label_name%5B%5D=8.+Developer+Docs)


